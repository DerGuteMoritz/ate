(define-page
  extension: "html" ;; (match-pages '((* extension: "html")))
  title: "A nice page"
  ;; this is the default
  render: (list default-content-renderer
                (part 'foo)))
