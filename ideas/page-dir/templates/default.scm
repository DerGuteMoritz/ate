(define-template (title content)
  `(html
    (head (title ,title))
    (body
     (header
      (h1 ,title))
     (section
      (@ (id "content"))
      ,content))))
